import unittest

import pyAgrum as gum
from .pyAgrumTestSuite import pyAgrumTestCase, addTests

import pyAgrum.ctbn as ct

class TestCtbnLearner(pyAgrumTestCase):
    def testLearner1(self):
        # create ctbn
        ctbn = ct.CTBN()
        A = gum.LabelizedVariable("cloudy?", "cloudy?", ["0", "1"])
        B = gum.LabelizedVariable("rain?", "rain?", ["0", "1"])
        ctbn.add(A)
        ctbn.add(B)
        ctbn.addArc("cloudy?", "rain?")
        ctbn.addArc("rain?", "cloudy?")
        ctbn.CIM("cloudy?")[{"rain?": "0"}] = [[-.1, .1],
                                             [1, -1]]
        ctbn.CIM("cloudy?")[{"rain?": "1"}] = [[-.1, .1],
                                             [1, -1]]
        ctbn.CIM("rain?")[{"cloudy?": "0"}] = [[-0.5, 0.5],
                                               [1000, -1000]]
        ctbn.CIM("rain?")[{"cloudy?": "1"}] = [[-2, 2],
                                               [2, -2]]
        
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory4.csv'), n=1, timeHorizon=1000, burnIn=500)
        learner = ct.Learner(self.agrumSrcDir('trajectory4.csv'))
        ctbnLearned = learner.learnCTBN(template=ctbn)
        
        # test if arcs are correctly learned
        self.assertSetEqual(set([(ctbn.name(n),ctbn.name(m)) for n,m in ctbn.arcs()]), set([(ctbnLearned.name(n),ctbnLearned.name(m)) for n,m in ctbnLearned.arcs()]))
        
    def testLearner2(self):
        # create ctbn
        ctbn = ct.CTBN()
        A = gum.LabelizedVariable("A", "A", ["a", "b"])
        B = gum.LabelizedVariable("B", "B", ["x", "y"])
        C = gum.LabelizedVariable("C", "C", ["m", "n"])
        ctbn.add(A)
        ctbn.add(B)
        ctbn.add(C)

        ctbn.addArc("A", "B")
        ctbn.addArc("B", "C")

        ctbn.CIM("A")[:] = [[-4, 4],
                            [2, -2]]
        ctbn.CIM("B")[{"A": "a"}] = [[-5, 5],
                                    [4, -4]]
        ctbn.CIM("B")[{"A": "b"}] = [[-1, 1],
                                    [1, -1]]
        ctbn.CIM("C")[{"B": "x"}] = [[-5, 5],
                                    [4, -4]]
        ctbn.CIM("C")[{"B": "y"}] = [[-1, 1],
                                    [1, -1]]

        # make sample
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory1_1.csv'), n=1, timeHorizon=2000, burnIn=500)
        learner = ct.Learner(self.agrumSrcDir('trajectory1_1.csv'))
        ctbnLearned = learner.learnCTBN(template=ctbn)
        
        # test if arcs are correctly learned
        self.assertSetEqual(set([(ctbn.name(n),ctbn.name(m)) for n,m in ctbn.arcs()]), set([(ctbnLearned.name(n),ctbnLearned.name(m)) for n,m in ctbnLearned.arcs()]))
        
    def testLearner3(self):
        # create random ctbn with 5 nodes with domainsize=3 and 2 parents at mot
        ctbn = ct.randomCTBN((1,2), n=5, parMax=2, modal=3)
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory7.csv'), timeHorizon=50, burnIn=10)
        learner = ct.Learner(self.agrumSrcDir('trajectory7.csv'))
        ctbnLearned = learner.learnCTBN()
        
        # test if variables and labels were correclty found
        self.assertEqual(len(ctbn.variables()), len(ctbnLearned.variables()))
        self.assertSetEqual(set(ctbn.names()), set(ctbnLearned.names()))
        for v in ctbn.variables():
            self.assertSetEqual(set(ctbn.labels(v.name())), set(ctbnLearned.labels(v.name())))
        
ts = unittest.TestSuite()
addTests(ts, TestCtbnLearner)