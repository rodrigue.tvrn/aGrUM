import unittest

import pyAgrum as gum
from .pyAgrumTestSuite import pyAgrumTestCase, addTests

import pyAgrum.ctbn as ct


class TestCtbnIndependence(pyAgrumTestCase):
    def testIndep1(self):
        # create ctbn
        ctbn = ct.CTBN()
        A = gum.LabelizedVariable("cloudy?", "cloudy?", ["0", "1"])
        B = gum.LabelizedVariable("rain?", "rain?", ["0", "1"])
        ctbn.add(A)
        ctbn.add(B)
        ctbn.addArc("cloudy?", "rain?")
        ctbn.addArc("rain?", "cloudy?")
        ctbn.CIM("cloudy?")[{"rain?": "0"}] = [[-.1, .1],
                                             [1, -1]]
        ctbn.CIM("cloudy?")[{"rain?": "1"}] = [[-.1, .1],
                                             [1, -1]]
        ctbn.CIM("rain?")[{"cloudy?": "0"}] = [[-0.5, 0.5],
                                               [1000, -1000]]
        ctbn.CIM("rain?")[{"cloudy?": "1"}] = [[-2, 2],
                                               [2, -2]]
        
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory4.csv'), n=1, timeHorizon=5000, burnIn=500)
        
        # load trajectory
        traj = ct.Trajectory(self.agrumSrcDir('trajectory4.csv'), ctbn=ctbn)
        # define independence test
        test = ct.FChi2Test(traj)
        
        # independence tests
        self.assertFalse(test.testIndep("rain?", "cloudy?", []))
        self.assertFalse(test.testIndep("cloudy?", "rain?", []))
        
    def testIndep2(self):
        # create ctbn
        ctbn = ct.CTBN()
        coinA = gum.LabelizedVariable("coinA", "coinA", ["head", "tail"])
        coinB = gum.LabelizedVariable("coinB", "coinB", ["head", "tail"])
        ctbn.add(coinA)
        ctbn.add(coinB)
        
        ctbn.CIM("coinA")[:] = [[-2, 2], [2, -2]]
        ctbn.CIM("coinB")[:] = [[-2, 2], [2, -2]]
        
        # generate sample
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory5.csv'), timeHorizon=1000, burnIn=500)
        traj = ct.Trajectory(self.agrumSrcDir('trajectory5.csv'), ctbn=ctbn)
        
        # test independence
        test = ct.FChi2Test(traj)
        self.assertTrue(test.testIndep("coinA", "coinB", []))
        self.assertTrue(test.testIndep("coinB", "coinA", []))
              
    def testIndep3(self):
        # create ctbn
        ctbn = ct.CTBN()
        A = gum.LabelizedVariable("A", "A", ["a", "b"])
        B = gum.LabelizedVariable("B", "B", ["x", "y"])
        C = gum.LabelizedVariable("C", "C", ["m", "n"])
        ctbn.add(A)
        ctbn.add(B)
        ctbn.add(C)

        ctbn.addArc("A", "B")
        ctbn.addArc("B", "C")

        ctbn.CIM("A")[:] = [[-4, 4],
                            [2, -2]]
        ctbn.CIM("B")[{"A": "a"}] = [[-5, 5],
                                    [4, -4]]
        ctbn.CIM("B")[{"A": "b"}] = [[-1, 1],
                                    [1, -1]]
        ctbn.CIM("C")[{"B": "x"}] = [[-5, 5],
                                    [4, -4]]
        ctbn.CIM("C")[{"B": "y"}] = [[-1, 1],
                                    [1, -1]]

        # make sample
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory1_1.csv'), n=1, timeHorizon=1000, burnIn=500)
        traj = ct.Trajectory(self.agrumSrcDir('trajectory1_1.csv'), ctbn=ctbn)
        
        # test independence
        test = ct.FChi2Test(traj)
        self.assertFalse(test.testIndep("B", "A", []))
        self.assertFalse(test.testIndep("C", "B", []))
        self.assertTrue(test.testIndep("A", "B", []))
        self.assertTrue(test.testIndep("C", "A", []))
        #self.assertFalse(test.testIndep("C", "A", ["B"]))
   
    def testIndep4(self):
        # create ctbn
        ctbn = ct.CTBN()
        X = gum.LabelizedVariable("X", "X", ["x0", "x1"])
        Y = gum.LabelizedVariable("Y", "Y", ["y0", "y1"])
        Z = gum.LabelizedVariable("Z", "Z", ["z0", "z1"])
        ctbn.add(X)
        ctbn.add(Y)
        ctbn.add(Z)
        ctbn.addArc("X", "Y")
        ctbn.addArc("Z", "Y")
        ctbn.CIM("X")[:] = [[-2, 2], [2, -2]]
        ctbn.CIM("Z")[:] = [[-2, 2], [2, -2]]
        ctbn.CIM("Y")[{"X": "x0", "Z": "z0"}] = [[-1, 1], [1, -1]]
        ctbn.CIM("Y")[{"X": "x1", "Z": "z0"}] = [[-3, 3], [3, -3]]
        ctbn.CIM("Y")[{"X": "x0", "Z": "z1"}] = [[-4, 4], [4, -4]]
        ctbn.CIM("Y")[{"X": "x1", "Z": "z1"}] = [[-5, 5], [5, -5]]
        
        # make sample
        ie = ct.ForwardSamplingInference(ctbn)
        ie.writeTrajectoryCSV(self.agrumSrcDir('trajectory6.csv'), timeHorizon=1000, burnIn=500)
        traj = ct.Trajectory(self.agrumSrcDir('trajectory6.csv'), ctbn=ctbn)
        
        # test independence
        test = ct.FChi2Test(traj)
        self.assertTrue(test.testIndep("X", "Z", []))
        self.assertFalse(test.testIndep("Y", "X", []))
        self.assertFalse(test.testIndep("Y", "Z", []))
        # self.assertFalse(test.testIndep("X", "Z", ["Y"]))
    
        
ts = unittest.TestSuite()
addTests(ts, TestCtbnIndependence)